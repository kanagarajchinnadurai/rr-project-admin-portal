import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { ActivateGuard} from './activate-guard';
const routes: Routes = [
  {path: '' , redirectTo: '/login' , pathMatch: 'full'},
  {path: 'login' , component: LoginComponent},
  { path: 'dashboard/:id' , component: DashboardComponent, canActivate: [ActivateGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
