import { Component, ViewChild, ElementRef } from '@angular/core';
import { Options } from 'ng5-slider';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'RR Admin Portal';
  constructor() { }
}
