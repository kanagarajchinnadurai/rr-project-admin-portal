import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse, HttpClient
} from '@angular/common/http';
import { Observable} from 'rxjs/Observable';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import {ApiserviceService} from './apiservice.service';

@Injectable()
export class AddTokenInterceptor implements HttpInterceptor {
  changableToken = '';
  constructor(private http: HttpClient, private route: Router, private api: ApiserviceService) {}
  additionalHeader: HttpRequest<any>;
  additionalHeaderTwo: HttpRequest<any>;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*    console.log(AddTokenInterceptor - ${req.url});*/

    this.additionalHeader = req.clone({
      setHeaders: {
        Authorization : localStorage.getItem('a_rtoken'),
        'auth-x-rr': localStorage.getItem('a_utoken'),
        id: localStorage.getItem('a_uid')
      }
    });

    return next.handle(this.additionalHeader).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
          window.alert('CLIENTSIDE');
        } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          if (error.status === 401) {
            this.api.getNewToken().subscribe( res => {
              // @ts-ignore
              localStorage.setItem('a_utoken' , res.token);  this.changableToken = res.token;
              this.additionalHeaderTwo = req.clone({
                setHeaders: {
                  Authorization : localStorage.getItem('a_rtoken'),
                  // @ts-ignore
                  'auth-x-rr': res.token,
                  id: localStorage.getItem('a_uid')
                }
              });
            } , () => {
              localStorage.clear();
              this.route.navigate(['/login']);
            } , () => {
            });
          } else if (error.status === 403) {
            localStorage.clear();
            this.route.navigate(['/login']);
          }
        }
        return next.handle(this.additionalHeaderTwo);
        /*  return throwError(errorMessage);*/
      }),
      retry(2)
    );
  }

}
