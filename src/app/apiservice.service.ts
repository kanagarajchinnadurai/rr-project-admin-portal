import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpBackend, HttpParams, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  /*baseURL = 'http://172.16.25.32:3000/';*/
  baseURL = 'https://backendapi.realresearcher.com/';
  /*baseURL = 'http://18.203.66.162:3000/';*/
  currentDocument = this.socket.fromEvent('responses');
  currentSurvey = this.socket.fromEvent('surveys');
  avoidhttp: HttpClient;
  canActivateLogin = true;
  constructor(public http: HttpClient, private socket: Socket , handler: HttpBackend) {
    this.avoidhttp = new HttpClient(handler);
  }

  getNewToken() {
    const headers = new HttpHeaders({
      Authorization : localStorage.getItem('a_rtoken'),
      'auth-x-rr': localStorage.getItem('a_utoken'),
      id: localStorage.getItem('a_uid')
    });
    return this.avoidhttp.get(this.baseURL + 'admin/getNewToken' , {headers});
  }

  doLogin(email, password) {
    return this.avoidhttp.post(this.baseURL + 'admin/login' , {email, password});
  }

  getSurvey() {
    return this.http.get( this.baseURL + 'admin/getSurvey');
  }

  getBudget() {
    return this.http.get( this.baseURL + 'admin/inquireBalance');
  }

  changeStatus(id , status) {
    return this.http.post(this.baseURL + 'admin/setSurveyStatus' , {id, status});
  }

  getSurveysFiltered(status) {
    return this.http.post(this.baseURL + 'admin/surveys?status=' + status, {} );
  }

  getpublishSurvey(id , budget) {
    return this.http.post(this.baseURL + 'admin/publishSurvey', {id , budget});
  }

  getSponsors() {
    return this.http.get( this.baseURL + 'admin/sponserList');
  }

  GetOneSurvey(id) {
    return this.http.get( this.baseURL + 'admin/responses/' + id);
  }

  getUsers() {
    return this.http.get( this.baseURL + 'admin/usersList');
  }

  FilteredUser(ObjectFilter) {
    return this.http.get( this.baseURL + 'admin/usersList');
  }

  setApproveSurveyStatus(id, status) {
    return this.http.post(this.baseURL + 'admin/setSurveyStatus', {id , status});
  }

  setRejectSurveyStatus(id, status, message) {
    return this.http.post(this.baseURL + 'admin/setSurveyStatus', {id , status, message});
  }

  SocketCheck(id) {
    console.log('Called' , id);
    this.socket.emit('getSurveyID' , {surveyid: id, userid: localStorage.getItem('a_uid')
    });
  }

  getCategories() {
    return this.http.get( this.baseURL + 'admin/categories');
  }

  addCategory(formData , type) {
    const fileDataE = new FormData();
    fileDataE.append('file', formData.file);
    fileDataE.append('name', formData.name);
    return this.http.post(this.baseURL + 'admin/addNewCategory', fileDataE);
  }

  deleteCategory(id) {
    return this.http.get( this.baseURL + 'admin/deleteCategory/' + id );
  }

  getData() {
    return this.http.get( this.baseURL + 'global/getData');
  }

  getCountriesList() {
    return this.http.get( this.baseURL + 'global/countriesList');
  }

  getLanguages() {
    return this.http.get( this.baseURL + 'global/languages');
  }


}
