import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiserviceService} from '../apiservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  myForm: FormGroup;
  loader = false; message = '';
  constructor(public route: Router, public api: ApiserviceService) {
    this.myForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    sessionStorage.clear();
    localStorage.clear();
    this.loader = false;
    this.message = '';
  }

  doLogin() {
    this.loader = true;
    this.api.doLogin(this.myForm.value.email , this.myForm.value.password).subscribe( res => {
      console.log(res);
      // @ts-ignore
      localStorage.setItem('a_uid', res.user.id);
      // @ts-ignore
      localStorage.setItem('a_utoken', res.token);
      // @ts-ignore
      localStorage.setItem('a_rtoken', res.refreshToken);
      this.route.navigate(['/dashboard/reports']);
      // @ts-ignore
    } , (err) => {
      // @ts-ignore
        this.message = err.error.mesaage; this.loader = false; } , () => {this.loader = false; });
  }
}
