import { Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ActivateGuard implements CanActivate {
  aUid;
  constructor(public route: Router) { this.aUid = localStorage.getItem('a_uid'); }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.aUid) {
      return true;
    } else {
      this.route.navigate(['/login']);
      return false;
    }
  }

}

