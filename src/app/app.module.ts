import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportsComponent } from './dashboard/reports/reports.component';
import { SurvaysComponent } from './dashboard/survays/survays.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { AdsComponent } from './dashboard/ads/ads.component';
import { RegisterComponent } from './register/register.component';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { PaymentRequestComponent } from './dashboard/payment-request/payment-request.component';
import {NgApexchartsModule} from 'ng-apexcharts';
import { UsersComponent } from './dashboard/users/users.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { Ng5SliderModule } from 'ng5-slider';
import { CategoriesComponent } from './dashboard/categories/categories.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AddTokenInterceptor} from './HttpRequestInterceptor';
import { ActivateGuard} from './activate-guard';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

export  function  HttpLoaderFactory(http: HttpClient) {
  return  new  TranslateHttpLoader(http, './assets/i18n/', '.json');
}
const config: SocketIoConfig = { url: 'https://backendapi.realresearcher.com/', options: {query: {auth_x_rr: localStorage.getItem('a_utoken')}} };
/*const config: SocketIoConfig = { url: 'http://18.203.66.162:3000/', options: {query: {auth_x_rr: localStorage.getItem('a_utoken')}} };*/

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    DashboardComponent,
    ReportsComponent,
    SurvaysComponent,
    AdsComponent,
    RegisterComponent,
    PaymentRequestComponent,
    UsersComponent,
    CategoriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModalModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgApexchartsModule,
    PDFExportModule,
    BrowserAnimationsModule,
    ChartsModule,
    Ng5SliderModule,
    NgxPaginationModule,
    SocketIoModule.forRoot(config),
    TranslateModule.forRoot({
      loader: {
        provide:  TranslateLoader,
        useFactory:  HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [TranslateModule],
  providers: [TranslateService, ActivateGuard,
    [{ provide: HTTP_INTERCEPTORS, useClass: AddTokenInterceptor, multi: true }]],
  bootstrap: [AppComponent]
})
export class AppModule { }
