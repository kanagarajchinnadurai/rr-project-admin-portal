import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiserviceService} from '../../apiservice.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class CategoriesComponent implements OnInit {
  @ViewChild('error', {static: false}) errorRef: ElementRef;
  myForm: FormGroup; fileName; fileType;  fileCheck;
  loader = false; message = ''; CategoryData;
  /*baseURL = 'http://18.203.66.162:3000/';*/
  baseURL = 'https://backendapi.realresearcher.com/';
  url; FormatType = null; rSearch = ''; imageFsize; imageFileSizeCheck;
  constructor(public route: Router , public api: ApiserviceService , private modalService: NgbModal) {
    this.myForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      file: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.api.getCategories().subscribe( res => {
      // @ts-ignore
      this.CategoryData = res.data.reverse();
    });
    this.myForm.reset();
    this.fileCheck = null;
  }

  onFileSelect(event) {
    this.fileCheck = null;
    if (event.target.files.length > 0) {
      this.FormatType = 'image';
      const file = event.target.files[0];
      this.fileName = file.name;
      this.fileType = file.type;
      this.imageFsize = file.size;
      if (this.imageFsize > 1048576) {
        this.imageFileSizeCheck = false;
      } else if (this.fileType === 'image/jpeg' || this.fileType === 'image/png') {
        this.fileCheck = true;
        this.imageFileSizeCheck = true;
      } else {
        this.fileCheck = false;
      }

      this.myForm.patchValue({file});
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      // @ts-ignore
      reader.onload = (events) => { // called once readAsDataURL is completed
        // @ts-ignore
        this.url = events.target.result;
      };
    }
    this.loader = false;
  }

  addCategory() {
    this.loader = true;
    this.api.addCategory(this.myForm.value , this.FormatType).subscribe(res => {
    }, (err) => {
      this.loader = false;
      // @ts-ignore
      this.message = err.error.mesaage;
    }, () => {
      this.myForm.reset();
      this.fileName = null;
      this.loader = false;
      this.ngOnInit();
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title' ,
      size: 'lg'}).result.then((result) => {
    });
  }

  deleteCategory(id) {
    this.api.deleteCategory(id).subscribe( res => {}, () => {
      this.open(this.errorRef);
    } , () => {
      this.ngOnInit();
    });
  }

}
