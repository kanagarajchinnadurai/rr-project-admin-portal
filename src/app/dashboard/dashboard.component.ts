import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApiserviceService} from '../apiservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  aUid;
  constructor(public route: Router, public api: ApiserviceService) {}

  ngOnInit() {
    this.aUid = localStorage.getItem('a_uid');
    if (!this.aUid) {
      this.route.navigate(['/']);
    }
  }

  doLogout() {
    localStorage.clear();
    this.route.navigate(['/']);
  }
}
