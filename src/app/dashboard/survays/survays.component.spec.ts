import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SurvaysComponent } from './survays.component';

describe('SurvaysComponent', () => {
  let component: SurvaysComponent;
  let fixture: ComponentFixture<SurvaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurvaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurvaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
