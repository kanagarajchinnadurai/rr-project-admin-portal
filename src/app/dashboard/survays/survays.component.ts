import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiserviceService} from '../../apiservice.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-survays',
  templateUrl: './survays.component.html',
  styleUrls: ['../dashboard.component.scss']
})

export class SurvaysComponent implements OnInit {
  @ViewChild('optionStack' , {static: false }) ansField: ElementRef;
  @ViewChild('questionField' , {static: false }) qField: ElementRef;
  closeResult = ''; ChosenCurrency = 'TNC'; searchParam = '';
  aUid; loader = false; message; selectedId; surveyArr = [];
  SelectedSurveyID; SelectedSurvey; myForm: FormGroup; rejectForm: FormGroup;
  CreateQuestionTemplate = []; filteredArr = []; SelectedQuestions;
  requestAllData; requestCountriesList; requestLanguages;
  baseURL = 'https://backendapi.realresearcher.com/';
  /*baseURL = 'http://18.203.66.162:3000/';*/

  constructor(private modalService: NgbModal, public route: Router, public api: ApiserviceService) {
    this.aUid = localStorage.getItem('a_uid');
    this.myForm = new FormGroup({
      fee: new FormControl(null, Validators.required)
    });
    this.rejectForm = new FormGroup({
      message: new FormControl(null, Validators.required)
    });
  }
  ngOnInit() {
    this.loader = true;
    window.scroll(0, 0);
    if (this.aUid) {
      this.api.getSurvey().subscribe(res => {
        // @ts-ignore
        for (let i = 0 ; i < res.data.length ; i++) {    // USER-LEVEL
            // @ts-ignore
            if (res.data[i].status !== 'publish' && res.data[i].status !== 'paid') {
              // @ts-ignore
              this.selectedId = res.data[0]._id;
            }
        }
        // @ts-ignore
        this.surveyArr = res.data.reverse();
        // @ts-ignore
      }, (err) => {
        this.message = err.error.mesaage;
        this.loader = false;
      }, () => {
        this.loader = false;
      });

     /* this.api.getData().subscribe( res => { this.requestAllData = res; console.log('requestAllData', res);
      }, () => {} , () => {});
      this.api.getCountriesList().subscribe( res => { this.requestCountriesList = res;  console.log('requestCountriesList', res);
      }, () => {} , () => {});
      this.api.getLanguages().subscribe( res => { this.requestLanguages = res; console.log('requestLanguages', res);
      }, () => {} , () => {});*/
    } else {
      this.route.navigate(['/login']);
    }
  }

  selectSurvey(id , questions , singleSurvey) {
    this.selectedId = id;
    this.CreateQuestionTemplate = [];
    this.SelectedSurveyID = id;
    this.filteredArr = [];
    this.SelectedSurvey = singleSurvey;
    this.SelectedQuestions = questions;
    console.log(this.SelectedSurvey);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  statusApproveChange(id) {
    this.api.setApproveSurveyStatus(id , 'survey_approved').subscribe( res => {
      this.ngOnInit();
    } , () => {
    } , () => { this.SelectedSurvey = null; });
  }

  statusRejectChange(id) {
    this.api.setRejectSurveyStatus(id , 'survey_rejected', this.rejectForm.value.message).subscribe( res => {
    this.ngOnInit();
    this.modalService.dismissAll();
    } , () => {
    } , () => { this.SelectedSurvey = null; });
  }


}

