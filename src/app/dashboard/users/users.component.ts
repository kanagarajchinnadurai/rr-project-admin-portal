import { Component, OnInit, NgModule } from '@angular/core';
import {ApiserviceService} from '../../apiservice.service';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class UsersComponent implements OnInit {
  userId;  requestData; aUid; searchParam = ''; timeDiff;
  closeResult = ''; crequestData; irequestData; dataResult; comprequestData;
  langArr; nationalityArr; cntryArr; cityArr; showsearch;
  gender; relationStatus; language; interestArr; interest; searchCompParam = '';
  ageArr; cuisinesArr; allDataCnt; compDataCnt; incompDataCnt;
  page = 1; pageSize = 10; public age: number; FullUserList;
  filterArr = {
    gender: '',
    age: '',
    relationStatus: '',
    language: '',
    nationality: '',
    country: '',
    city: '',
    interest: '',
    cuisines: '',
    revenue: 0,
    minAge: 1,
    maxAge: 200
  };
  AudienceLimit = 0;
  minValue: number = 1000;
  maxValue: number = 100000;
  options: Options = {
    floor: 0,
    ceil: 100000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$' + value;
        case LabelType.High:
          return '$' + value;
        default:
          return '$' + value;
      }
    }
  };

  ageoptions: Options = {
    floor: 1,
    ceil: 200,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '' + value;
        case LabelType.High:
          return '' + value;
        default:
          return '' + value;
      }
    }
  };

  constructor(private modalService: NgbModal, public route: Router, public api: ApiserviceService) {
    this.showsearch = true;
  }
  public CalculateAge(value) {
    if (value) {
      var timeDiff = Math.abs(Date.now() - new Date(value).getTime());
      return Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
  }
  ngOnInit() {
    this.aUid = localStorage.getItem('a_uid');
    this.langArr = []; this.nationalityArr = []; this.cntryArr = []; this.cityArr = [];
    this.interestArr = []; this.cuisinesArr = []; this.ageArr = []; this.dataResult = 1;
    this.crequestData = []; this.irequestData = []; this.comprequestData = [];
    if (this.aUid) {
      this.api.getUsers().subscribe( res => {
        // @ts-ignore
        this.requestData = res.data.reverse(); this.FullUserList = res.data.reverse();
        // @ts-ignore
        this.allDataCnt = res.data.length;

        for (let i = 0; i < this.requestData.length; i++) {
          if (this.requestData[i].kycStatus && this.requestData[i].kycStatus === 'completed') {
            this.crequestData.push(this.requestData[i]);
            this.comprequestData.push(this.requestData[i]);
          }
        }
        // @ts-ignore
        this.compDataCnt = this.crequestData.length;
        /*console.log('compDataCnt', this.compDataCnt);*/

        for (let i = 0; i < this.requestData.length; i++) {
          // @ts-ignore
          if (!this.requestData[i].kycStatus) {
            this.irequestData.push(this.requestData[i]);
          }
          // @ts-ignore
          if (this.requestData[i].kycStatus && this.requestData[i].kycStatus !== 'completed') {
            this.irequestData.push(this.requestData[i]);
          }
        }
        // @ts-ignore
        this.incompDataCnt = this.irequestData.length;

        /*console.log('requestData' , this.requestData);*/
        for (let i = 0; i < this.requestData.length; i++) {
          if (this.requestData[i].languages && this.requestData[i].languages.length > 0) {
            for (let x of this.requestData[i].languages) {
              this.langArr.push(x);
            }
            this.langArr = this.langArr.filter((item , index) => this.langArr.indexOf(item) === index);
          }
          if (this.requestData[i].interest && this.requestData[i].interest.length > 0){
            for (let y of this.requestData[i].interest) {
              this.interestArr.push(y);
            }
            this.interestArr = this.interestArr.filter((item , index) => this.interestArr.indexOf(item) === index);
          }
          /*console.log('INT' , this.interestArr);*/

          if (this.requestData[i].cuisines && this.requestData[i].cuisines.length > 0) {
            for (let a of this.requestData[i].cuisines) {
              this.cuisinesArr.push(a);
            }
            this.cuisinesArr = this.cuisinesArr.filter((item , index) => this.cuisinesArr.indexOf(item) === index);
          }

          if (this.requestData[i].nationality) {
            this.nationalityArr.push(this.requestData[i].nationality);
            /*console.log(this.nationalityArr);*/
          }

          if (this.requestData[i].country && this.requestData[i].country.length > 0) {
            this.cntryArr.push(this.requestData[i].country);
          }

          if (this.requestData[i].city) {
            this.cityArr.push(this.requestData[i].city);
          }

          if (this.requestData[i].age) {
            this.timeDiff = Math.abs(Date.now() - new Date(this.requestData[i].age).getTime());
            this.age = Math.floor(this.timeDiff / (1000 * 3600 * 24) / 365.25);
            this.ageArr.push(this.age);
          }
          this.nationalityArr = this.nationalityArr.filter((item , index) => this.nationalityArr.indexOf(item) === index);
          this.cntryArr = this.cntryArr.filter((item , index) => this.cntryArr.indexOf(item) === index);
          this.cityArr = this.cityArr.filter((item , index) => this.cityArr.indexOf(item) === index);
         /* console.log(this.ageArr);*/
        }
      }, () => {} , () => { });

    } else {
      this.route.navigate(['/login']);
    }
  }

  allData() {
    this.api.getUsers().subscribe( res => {
      // @ts-ignore
      this.requestData = res.data.reverse(); this.FullUserList = res.data.reverse();
      // @ts-ignore
      this.allDataCnt = res.data.length;
      /*console.log('requestData', this.requestData);*/
    }, () => {} , () => {this.dataResult = 1; this.showsearch = true; });
  }

  filterData() {
    /*console.log(this.searchParam);*/
    this.requestData = this.FullUserList.filter(x => x.email.includes(this.searchParam));
  }

  filterAllData() {
    this.completeData();
    this.crequestData = this.comprequestData.filter(x => x.email.includes(this.searchCompParam) || x.contact.includes(this.searchCompParam));
    /*console.log(this.crequestData);*/
  }

  completeData() {
    this.dataResult = 2;
    this.showsearch = false;
    this.api.getUsers().subscribe( res => {
    this.crequestData = []; this.comprequestData = [];
    // @ts-ignore
    this.requestData = res.data.reverse();
    for (let i = 0; i < this.requestData.length; i++) {
      if (this.requestData[i].kycStatus && this.requestData[i].kycStatus === 'completed') {
        this.crequestData.push(this.requestData[i]);
        this.comprequestData.push(this.requestData[i]);
      }
    }
    this.crequestData = this.comprequestData.filter(x => x.email.includes(this.searchCompParam) || x.contact.includes(this.searchCompParam));
    /*console.log(this.crequestData);*/
    // @ts-ignore
    this.compDataCnt = this.crequestData.length;
    /*console.log('crequestData', this.crequestData);*/
  }, () => {} , () => {});
  }

  inCompleteData() {
    this.api.getUsers().subscribe( res => {
    this.irequestData = [];
    // @ts-ignore
    this.requestData = res.data.reverse();
    for (let i = 0; i < this.requestData.length; i++) {
      // @ts-ignore
      if (!this.requestData[i].kycStatus) {
        this.irequestData.push(this.requestData[i]);
      }
      // @ts-ignore
      if (this.requestData[i].kycStatus && this.requestData[i].kycStatus !== 'completed') {
        this.irequestData.push(this.requestData[i]);
      }
    }
    // @ts-ignore
    this.incompDataCnt = this.irequestData.length;
    /*console.log('irequestData', this.irequestData);*/
  }, () => {} , () => {this.dataResult = 3; this.showsearch = false; });
  }

  getusers(value) {
    this.userId = value;
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  AudienceLimitRefresh() {
    // @ts-ignore
    this.api.FilteredUser(this.FilterTemplate).subscribe( res => {this.AudienceLimit = res.data; });
  }

  openAudience(content , selectedO) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title' , size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.AudienceLimitRefresh();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
