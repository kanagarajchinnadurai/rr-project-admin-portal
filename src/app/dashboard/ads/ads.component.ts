import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiserviceService} from '../../apiservice.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class AdsComponent implements OnInit {
  sponsorId;  requestData; aUid; searchParam = '';
  constructor(public route: Router, public api: ApiserviceService) {
    this.aUid = localStorage.getItem('a_uid');
    if (!this.aUid) {
      this.route.navigate(['/']);
    }
  }

  ngOnInit() {
    if (this.aUid) {
      this.api.getSponsors().subscribe( res => {
        // @ts-ignore
        this.requestData = res.data.reverse();
      }, () => {} , () => {});
    } else {
      this.route.navigate(['/login']);
    }
  }
}
