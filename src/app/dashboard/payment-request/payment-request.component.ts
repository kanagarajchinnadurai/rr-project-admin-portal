import { Component, OnInit, Input, NgModule } from '@angular/core';
import {ApiserviceService} from '../../apiservice.service';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-payment-request',
  templateUrl: './payment-request.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class PaymentRequestComponent implements OnInit {
  closeResult; myForm: FormGroup; rejectForm: FormGroup;
  requestData; aUid; searchParam = '';
  ShowBudget: any = 0;
  constructor(public route: Router, public api: ApiserviceService , public modalService: NgbModal) {
    this.aUid = localStorage.getItem('a_uid');
    if (!this.aUid) {
      this.route.navigate(['/']);
    }
    this.myForm = new FormGroup({
      budget: new FormControl(null, Validators.required)
    });
    this.rejectForm = new FormGroup({
      message: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    if (this.aUid) {
      this.api.getSurveysFiltered('paid').subscribe(res => {
        // @ts-ignore
        this.requestData = res.data;
      }, () => {}, () => {
      });
    } else {
      this.route.navigate(['/login']);
    }
    this.getBudget();
  }
  statusInfo(id , content) {
    this.modalService.open(content);
  }
  getBudget() {
    this.api.getBudget().subscribe(res => {
      this.ShowBudget = res;
    });
  }
  statusChange(id , content) {
    this.api.getpublishSurvey(id , this.myForm.value.budget.toString()).subscribe( res => {
    } , () => {
    } , () => {
      this.ngOnInit();
    });
  }
  open(content , selectedO) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  statusRejectChange(id , content) {
    this.api.setRejectSurveyStatus(id , 'reject_publish', this.rejectForm.value.message).subscribe( res => {
      this.ngOnInit();
    } , () => {
      this.modalService.open(content);
    } , () => {});
  }

}
