import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ApiserviceService} from '../../apiservice.service';
import { ChartComponent } from 'ng-apexcharts';
import { ApexNonAxisChartSeries, ApexResponsive, ApexChart, ApexFill, ApexDataLabels, ApexLegend} from 'ng-apexcharts';
import {ActivatedRoute, Router} from '@angular/router';
import {ChartOptions, ChartType} from 'chart.js';
import {Label, MultiDataSet, SingleDataSet, ThemeService} from 'ng2-charts';
export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
};
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['../dashboard.component.scss'],
  providers: [ThemeService]
})
export class ReportsComponent implements OnInit {
  @ViewChild('chart' , {static: false}) chart: ChartComponent;
  @ViewChild('htmlData' , {static: false}) htmlData: ElementRef;
  @ViewChild('pieChart101' , {static: false}) pieChart101: ElementRef;
  public chartOptions: Partial<any>; public chartOptionsBr: Partial<any>;
  loader; aUid; message; mysurveylist; SelectedQId; SelectedQuestion;
  OneSurvey; SelectedQuestions; SelectedSurveyID; SelectedSurvey;
  CreateQuestionTemplate = []; filteredArr = []; pieChartSeries = []; pieChartData = [];
  showChart = false; searchParam = ''; showtableResult = true; SelectedSurveyTitle;
  shorArea = false; currentDate = Date.now();
  public breakParagraphs = false; temp = {};
  baseURL = 'https://backendapi.realresearcher.com/';

  public get keepTogether(): string {
    return this.breakParagraphs ? '' : '.avoid-break';
  }
  // Doughnut
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public doughnutChartLabels: Label[];
  public doughnutChartData: SingleDataSet = [];
  public doughnutChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartPlugins = ['label1' , 'label2' , 'lable3'];
  CounterData: any = [];
  constructor(public route: Router, public api: ApiserviceService,  private aroute: ActivatedRoute) {
    this.aUid = localStorage.getItem('a_uid');
    if (!this.aUid) {
      this.route.navigate(['/login']);
    }
    this.chartOptions = {
      series: [0, 0, 0, 0, 0],
      chart: {
        width: 500,
        type: "donut",
        redrawPaths: true,
        updateSyncedCharts: true
      },
      labels: {
        show: true,
        name: {
          show: true,
          fontSize: '22px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontWeight: 600,
          color: undefined,
          offsetY: -10
        }
      },
      dataLabels: {
        enabled: true,
        show: true
      },
      fill: {
        type: "gradient"
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
    this.chartOptionsBr = {
      series: [{data : [1, 3, 5, 2]}],
      chart: {
        width: 500,
        type: "bar",
        redrawPaths: true,
        updateSyncedCharts: true,
        animations: {
          dynamicAnimation: {
            enabled: true
          }
        }
      },
      labels: {
        show: true,
      },
      dataLabels: {
        enabled: true,
        show: true
      }
    };
  }

  ngOnInit() {
    if (this.aUid) {
      this.api.getSurveysFiltered('publish').subscribe( res => {
        // @ts-ignore
        this.mysurveylist = res.data.reverse();
      }, (err) => {
        this.loader = false;
        // @ts-ignore
        this.message = err.error.mesaage;
        } , () => {this.loader = false; });
    }
  }

  SelectedOneSurvey(id , questions , singleSurvey, title) {
    this.CreateQuestionTemplate = []; this.filteredArr = [];
    this.SelectedSurveyID = id; this.SelectedSurvey = singleSurvey;
    this.SelectedQuestions = questions;
    this.SelectedSurveyTitle = title;
    console.log(this.SelectedQuestions);
    this.showChart = true; this.showtableResult = false;
    this.api.GetOneSurvey(id).subscribe( res => {
      for (let z = 0 ; z < this.SelectedQuestions.length; z++) {
        for (let c = 0 ; c < this.SelectedQuestions[z].answers.length ; c ++) {
          this.CreateQuestionTemplate.push({Q: this.SelectedQuestions[z].q_id.toString(),  A: this.SelectedQuestions[z].answers[c].a_id.toString() , V: -1 , N: this.SelectedQuestions[z].answers[c].answer});
        }
      }
      // @ts-ignore
      this.OneSurvey = res.data;
      // @ts-ignore
      for (let i = 0 ; i < res.data.length ; i++) {    // USER-LEVEL
        // @ts-ignore
        for (let a = 0; a < res.data[i].response.length ; a++) {  // ANSWERS-LEVEL
          // @ts-ignore
          this.CreateQuestionTemplate.push({Q: res.data[i].response[a].qid.toString(),  A: res.data[i].response[a].aid.toString() , V: 0});
        }
      }

      for(let o = 0 ; o < this.CreateQuestionTemplate.length ; o ++) {
        for(let k = 0 ; k < this.CreateQuestionTemplate.length ; k ++) {
          if (this.CreateQuestionTemplate[o].Q === this.CreateQuestionTemplate[k].Q && this.CreateQuestionTemplate[o].A === this.CreateQuestionTemplate[k].A) {
            this.CreateQuestionTemplate[o].V += 1;
          }
        }
      }
      this.filteredArr  = this.CreateQuestionTemplate.reduce((acc, current) => {
        const x = acc.find(item => item.Q === current.Q && item.A === current.A);
        if (!x) {
          return  acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      this.SelectedQId = id;
      this.SelectedQuestion = name;

      this.GenerateGraphs(this.SelectedQuestions[0].q_id, this.SelectedQuestions[0].question);
      this.CreatePattern();
      this.ReUpdate();
    });
  }

  GenerateGraphs(id, question) {
    this.pieChartData = []; this.pieChartSeries = [];
    this.showChart = true; this.showtableResult = false;
    this.SelectedQId = id; this.SelectedQuestion = question;
    this.chartOptionsBr.series = [];
    this.CreatePattern();
  }

  injectValues() {
    return [4, 4, 6];
  }

  CreatePattern() {
    this.temp = [];
    let tempErray = [];
    let tempSeries = [];
    let tempValues = [];
    let tempWithPercentage = [];
    this.pieChartData = [];
    this.doughnutChartLabels = [];
    this.doughnutChartData = [];
    let floatpercent;

    for (const x of this.SelectedQuestions) {
      let l = 1;
      for (const i of this.filteredArr) {
        if (x.q_id.toString() === i.Q.toString()) {
          tempErray.push({V: i.V , N: i.N});
          tempSeries.push(i.N);
          tempValues.push(i.V);
          floatpercent = i.V / this.SelectedSurvey.counter * 100;
          tempWithPercentage.push( l++ + ' --- ' + (Number.parseFloat(floatpercent).toFixed(2)) + ' %' );
        }
      }
      this.chartOptionsBr.series[x.question]  = [{data: tempValues}];
      this.doughnutChartLabels[x.question] = tempWithPercentage;
      this.doughnutChartData[x.question] = tempValues;
      this.temp[x.question] = tempErray;
      this.pieChartSeries[x.question] = tempSeries;
      this.pieChartData[x.question] = tempValues;
      tempWithPercentage = [];
      tempErray = [];
      tempSeries = [];
      tempValues = [];
    }
    console.log('Incredible', tempWithPercentage);
    this.shorArea = true;
  }

  ReUpdate() {
    this.api.currentDocument.subscribe( res => {
      if (res[0].surveyId === this.SelectedSurvey._id){

        this.CreateQuestionTemplate = [];
        this.filteredArr = [];
        for (let z = 0 ; z < this.SelectedQuestions.length; z++) {
          for (let c = 0 ; c < this.SelectedQuestions[z].answers.length ; c ++) {
            this.CreateQuestionTemplate.push({Q: this.SelectedQuestions[z].q_id.toString(),  A: this.SelectedQuestions[z].answers[c].a_id.toString() , V: -1 , N: this.SelectedQuestions[z].answers[c].answer});
          }
        }
        // @ts-ignore
        this.OneSurvey = res;
        // @ts-ignore
        for (let i = 0 ; i < res.length ; i++) {    // USER-LEVEL
          // @ts-ignore
          for (let a = 0; a < res[i].response.length ; a++) {  // ANSWERS-LEVEL
            // @ts-ignore
            this.CreateQuestionTemplate.push({Q: res[i].response[a].qid.toString(),  A: res[i].response[a].aid.toString() , V: 0});
          }
        }
        for(let o = 0 ; o < this.CreateQuestionTemplate.length ; o ++){
          for(let k = 0 ; k < this.CreateQuestionTemplate.length ; k ++) {
            if (this.CreateQuestionTemplate[o].Q === this.CreateQuestionTemplate[k].Q && this.CreateQuestionTemplate[o].A === this.CreateQuestionTemplate[k].A) {
              this.CreateQuestionTemplate[o].V += 1;
            }
          }
        }
        this.filteredArr  = this.CreateQuestionTemplate.reduce((acc, current) => {
          const x = acc.find(item => item.Q === current.Q && item.A === current.A);
          if (!x) {
            return  acc.concat([current]);
          } else {
            return acc;
          }
        }, []);
        this.ChangeValuesOnGraph();
      }
    });
    this.api.currentSurvey.subscribe( res => {
      this.CounterData = res;
      for (const x of this.CounterData) {
        for (let i = 0 ; i < this.mysurveylist.length ; i ++) {
          if (x._id === this.mysurveylist[i]._id) {
            this.mysurveylist[i].counter = x.counter;
          }
        }
      }
    });
  }
  ChangeValuesOnGraph() {
    this.temp = [];
    let tempValues = [];
    let tempWithPercentage = [];
    this.doughnutChartLabels = [];
    this.doughnutChartData = [];
    for (const x of this.SelectedQuestions) {
      let l = 1;
      let floatpercent;
      for (const i of this.filteredArr) {
        if (x.q_id.toString() === i.Q.toString()) {
          tempValues.push(i.V);
          floatpercent = i.V / this.OneSurvey.length * 100;
          tempWithPercentage.push( l++ + ' --- ' + (Number.parseFloat(floatpercent).toFixed(2)) + ' %' );
        }
      }

      this.chartOptionsBr.series[x.question]  = [{data: tempValues}];
      this.pieChartData[x.question] = tempValues;
      this.doughnutChartLabels[x.question] = tempWithPercentage;
      this.doughnutChartData[x.question] = tempValues;
      tempWithPercentage = [];
      tempValues = [];
    }
    this.shorArea = true;
  }

  clearGraphs() {
    console.log('nyak');
  }

}
